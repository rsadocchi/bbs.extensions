﻿using bbs.Extensions.Services;
using System.Globalization;
using System.Text.RegularExpressions;

namespace bbs.Extensions.Validations
{
    public static class Ext_Validation
    {
        public static bool ContainNumbers(this string source) { return Regex.IsMatch(source, @"\d"); }

        public static bool ContainsUpperCase(this string source) { return Regex.IsMatch(source, @"[A-Z]"); }

        public static string ToFormattedMoneyString(this decimal val, CultureInfo culture = null)
        {
            var rtn = string.Empty;
            if (culture == null) culture = CultureInfo.CurrentUICulture;
            rtn = string.Format(new Formatter_service(culture), "{0:C2}", val);
            return rtn;
        }

        public static bool IsValidEmailAddress(this string email) { return (new Validator_service()).MailAddress(email); }
    }
}
