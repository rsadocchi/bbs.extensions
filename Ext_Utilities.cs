﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace bbs.Extensions
{
    public static class Ext_Utilities
    {
        public static string ToMD5Hash(this string source)
        {
            if (string.IsNullOrWhiteSpace(source)) return null;
            var sb = new StringBuilder();
            var csp = new MD5CryptoServiceProvider();
            var bytes = csp.ComputeHash(Encoding.UTF8.GetBytes(source));
            bytes.ToList().ForEach(b => { sb.Append(b.ToString("x2")); });
            return sb.ToString();
        }

        public static byte[] ToByteArray(this Stream stream)
        {
            byte[] output = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(output, 0, output.Length)) > 0)
                    ms.Write(output, 0, read);
                return ms.ToArray();
            }
        }

        public static byte[] ToByteArray(this string source, Encoding encoding = null)
        {
            if (encoding == null) encoding = Encoding.UTF8;
            return encoding.GetBytes(source);
        }

        public static Stream ToStream(this string source, Encoding encoding = null)
        {
            byte[] byteArray = source.ToByteArray(encoding);
            return new MemoryStream(byteArray);
        }

        public static async Task<string> ToStringBackAsync(this byte[] source)
        {
            string text = null;
            using (var ms = new MemoryStream(source))
            using (var sr = new StreamReader(ms))
                text = await sr.ReadToEndAsync();
            return await Task.FromResult(text);
        }

        public static async Task<string> ToStringBackAsync(this Stream source)
        {
            string text = null;
            using (var sr = new StreamReader(source))
                text = await sr.ReadToEndAsync();
            return await Task.FromResult(text);
        }
    }
}
