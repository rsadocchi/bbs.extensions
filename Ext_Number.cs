﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bbs.Extensions
{
    public static class Ext_Number
    {
        public static bool IsEven(this int num) { return (num % 2 == 0 ? true : false); }
        public static bool IsEven(this long num) { return (num % 2 == 0 ? true : false); }
        public static bool IsEven(this double num) { return (num % 2 == 0 ? true : false); }
        public static bool IsEven(this float num) { return (num % 2 == 0 ? true : false); }
        public static bool IsEven(this short num) { return (num % 2 == 0 ? true : false); }
        public static bool IsEven(this decimal num) { return (num % 2 == 0 ? true : false); }
    }
}
