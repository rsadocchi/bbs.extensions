﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace bbs.Extensions.Enum
{
    [AttributeUsage(AttributeTargets.Enum | AttributeTargets.Field, AllowMultiple = false, Inherited = false)]
    public class EnumCustomValue : Attribute
    {
        public object Value { get; set; }
        public EnumCustomValue(object value) { Value = value; }
    }

    public static class Ext_Enum
    {
        public static TValue GetCustomValue<TSource, TValue>(this TSource source)
            where TSource : struct, IConvertible
            where TValue : Type
        {
            if (!typeof(TSource).IsEnum) return null;
            var memberInfo = typeof(TSource).GetField(System.Enum.GetName(typeof(TSource), source));
            var value = (EnumCustomValue)Attribute.GetCustomAttribute(memberInfo, typeof(EnumCustomValue));
            if (value == null) return null;
            return (TValue)value.Value;
        }

        public static IEnumerable<TSource> GetValues<TSource>(this TSource @enum)
            where TSource : struct, IConvertible
        {
            if (!typeof(TSource).IsEnum) return null;
            return System.Enum.GetValues(typeof(TSource)).Cast<TSource>();
        }

        public static IEnumerable<string> GetNames<TSource>(this TSource @enum)
            where TSource : struct, IConvertible
        {
            if (!typeof(TSource).IsEnum) return null;
            return System.Enum.GetNames(typeof(TSource)).AsEnumerable();
        }

        public static string GetName<TSource>(this TSource @enum, object value)
            where TSource : struct, IConvertible
        {
            if (!typeof(TSource).IsEnum) return null;
            return System.Enum.GetName(typeof(TSource), value);
        }
    }
}
